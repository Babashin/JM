import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class Test4 {
    public static void main(String[] args) {
        byte[] example = {72, 101, 108, 108, 111, 33};
        AsciiCharSequence answer = new AsciiCharSequence(example);
        System.out.println("Последовательность - "  + answer.toString());//Hello!
        System.out.println("Размер её - " +  answer.length());//6
        System.out.println("Символ под № 1 - " +  answer.charAt(1));//e
        System.out.println("Подпоследовательность - " +  answer.subSequence(1, 5));//ello

        //проверка на нарушение инкапсуляции private поля
        System.out.println(answer.toString());//Hello!
        example[0] = 74;
        System.out.println(answer.toString());//Hello!
    }
    public static class AsciiCharSequence implements CharSequence{
        byte[] bytes;

        public AsciiCharSequence(byte[] bytes) {
            this.bytes = bytes;
        }

        @Override
        public int length() {
            return bytes.length;
        }

        @Override
        public char charAt(int index) {
            return (char)bytes[index];
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            byte[]bytes1 = new byte[end-start];
            int j = 0;
            for (int i = start; i < end; i++) {
                bytes1[j] = bytes[i];
                j++;
            }
            return new AsciiCharSequence(bytes1);
        }

        @Override
        public String toString() {
            return new String(bytes);
        }
    }

}
