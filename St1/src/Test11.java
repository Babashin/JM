public class Test11 {
    @FunctionalInterface
    public interface NumberGenerator<T extends Number> {
        boolean cond(T arg);
    }

    public static NumberGenerator<? super Number> getGenerator() {
        return (arg)  -> arg.intValue() > 0;
    }

    public static void main(String[] args) {
        System.out.println(getGenerator().cond(1000));
    }
}
