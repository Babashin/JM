package TextAnalyzerPack;

public class TooLongTextAnalyzer implements TextAnalyzer {
    private int maxLength;

    public TooLongTextAnalyzer(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public Label processText(String string) {
        if(string.length()>maxLength){
            return Label.TOO_LONG;
        }
        return Label.OK;
    }
}
