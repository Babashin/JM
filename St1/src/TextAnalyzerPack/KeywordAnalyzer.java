package TextAnalyzerPack;

abstract public class KeywordAnalyzer implements TextAnalyzer {
    abstract protected String[] getKeywords();
    abstract protected Label getLabel();

    @Override
    public Label processText(String string) {
        String[] arrayString = getKeywords();
        for (String s : arrayString) {
            if(string.contains(s)){
                return getLabel();
            }
        }
        return Label.OK;
    }
}
