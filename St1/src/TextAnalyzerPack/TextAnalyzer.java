package TextAnalyzerPack;

public interface TextAnalyzer {
    Label processText(String string);
}
