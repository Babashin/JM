package TextAnalyzerPack;

public class NegativeTextAnalyzer extends KeywordAnalyzer {
    String[] smail = new String[]{":(", "=(", ":|"};

    @Override
    protected String[] getKeywords() {
        return smail;
    }

    @Override
    protected Label getLabel() {
        return Label.NEGATIVE_TEXT;
    }
}
