import java.util.function.Function;
import java.util.function.Predicate;

public class Test12 {
    public <T, U> Function<T, U> ternaryOperator(
            Predicate<? super T> condition,
            Function<? super T, ? extends U> ifTrue,
            Function<? super T, ? extends U> ifFalse) {
        return (con) ->  condition.test(con) ? ifTrue.apply(con) : ifFalse.apply(con);
    }

}
