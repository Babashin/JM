import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Test7 {
    public static void main(String[] args) {
        Scanner cs = new Scanner(System.in);
        Stack<Integer> list = new Stack<>();
        while (cs.hasNext()) {
            cs.next();
            if (cs.hasNextInt()) {
                list.add(cs.nextInt());
            }
        }
        for (int i : list) {
            System.out.print(i + " ");
        }
    }

    //1 2 3 4 5 6 7 8 9 10
}
