import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class Test15 {
    public static void main(String[] args) throws IOException {
        Comparator<Map.Entry<String,Long>> comp = (o1, o2) -> {
            Long l1 = o1.getValue();
            Long l2 = o2.getValue();
            int frequencyComp = l1.compareTo(l2);
            if(frequencyComp != 0) {
                return -frequencyComp;
            }
            String s1 = o1.getKey();
            String s2 = o2.getKey();
            return s1.compareTo(s2);
        };
        List<String> lines = new LinkedList<>();
        String line;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException ignored) {
        }
///////////////////////////////////////////////////////////////////////////////////////
        lines.stream().map(s -> s.replaceAll("[^A-Za-zА-Яа-я0-9'\\s]++", " "))
                .map(s->s.split("\\s++"))
                .flatMap(Arrays::stream)
                .map(String::toLowerCase)
                .map(s -> s.replaceAll(" ", ""))
                .map(s -> s.replaceAll("^[A-Za-zА-Яа-я]$", ""))
                .filter((s)->!s.equals(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .sorted(comp)
                .limit(10)
                .distinct()
                .forEach(w -> System.out.println(w.getKey()));
    }

static class Car{
    @Override
    public String toString() {
        return "Car";
    }
}
}
