import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test14 {
    public static void main(String[] args) {
        new BufferedReader(new InputStreamReader(System.in)).lines().
                map(String::toLowerCase).
                flatMap(s -> Arrays.stream(s.split("[^a-zа-я0-9']+"))).
                collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).
                //str -> str == Function.identity() что получили то и отдали, а второй элемент в мап частота повторений
                //количество повторений слова - значение, само слово ключ, на выходе Map
                entrySet().
                //набор ключ значание
                stream().
                sorted(Map.Entry.comparingByKey()).
                //сортировка по алфавиту
                sorted(Comparator.comparing((Function<Map.Entry<String, Long>, Long>) Map.Entry::getValue).reversed()).
                //выбрали ключ для компаратора и сравнили по нему и отсортировали, потом реверс
                        //у каждого объекта Map.Entry вызываем getValue ((s) -> getValue(s)) или что то похожее
                limit(10).
                map(Map.Entry::getKey).
                forEach(System.out::println);//(s)->System.out.println(s)
    }
}
//Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales consectetur purus at faucibus. Donec mi quam, tempor vel ipsum non, faucibus suscipit massa. Morbi lacinia velit blandit tincidunt efficitur. Vestibulum eget metus imperdiet sapien laoreet faucibus. Nunc eget vehicula mauris, ac auctor lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel odio nec mi tempor dignissim.