import java.util.Arrays;
import java.util.Objects;

public class Test5 {
    public static void main(String[] args)  {
        Pair<Integer, String> pair = Pair.of(1, "hello");
        Integer i = pair.getFirst(); // 1
        System.out.println(i);
        String s = pair.getSecond(); // "hello"
        System.out.println(s);
        Pair<Integer, String> pair2 = Pair.of(1, "hello");
        boolean mustBeTrue = pair.equals(pair2); // true!
        System.out.println(mustBeTrue);
        boolean mustAlsoBeTrue = pair.hashCode() == pair2.hashCode(); // true!
        System.out.println(mustAlsoBeTrue);
    }
    public static class Pair<T,N> {
        private final T t;
        private final N n;

        public Pair(T t, N n) {
            this.t = t;
            this.n = n;
        }

        public static <T,N>Pair<T,N> of(T i, N s){
            return new Pair<>(i, s);
        }
        public N getSecond(){
            return n;
        }
        public T getFirst(){
            return t;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair<?, ?> pair = (Pair<?, ?>) o;
            return Objects.equals(t, pair.t) && Objects.equals(n, pair.n);
        }

        @Override
        public int hashCode() {
            return Objects.hash(t, n);
        }
    }
}

