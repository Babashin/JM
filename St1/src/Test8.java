import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Test8 {
    public static Map<String, Long> getSalesMap(Reader reader) {
        Map<String, Long> map = new HashMap<>();
        try (BufferedReader red = new BufferedReader(reader)) {
            while (red.ready()) {
                String[] vvod = red.readLine().split(" ");
                map.merge(vvod[0], Long.valueOf(vvod[1]), (oldVal, newVal) -> oldVal + newVal);
            }
        } catch (IOException e) {
        }
        return map;
    }

    public static void main(String[] args) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream("Алексей 3000\nДмитрий 9000\nАнтон 3000\nАлексей 7000\nАнтон 8000".getBytes());
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(bais));
        System.out.println(getSalesMap(buffReader));
    }
}

