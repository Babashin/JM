import java.util.function.IntUnaryOperator;
import java.util.function.UnaryOperator;

public class Test10 {
    public UnaryOperator<Integer> sqrt() {
        return x -> x * x;
    }

    public static void main(String[] args) {
        Test10 test10 = new Test10();
        System.out.println(test10.sqrt().apply(5));
    }
}
