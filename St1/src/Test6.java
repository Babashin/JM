import java.util.Arrays;
import java.util.Optional;

public class Test6 {
    public static void main(String[] args) {
        DynamicArray<String> strings = new DynamicArray<>();
        strings.add("one");
        strings.add("two");
        strings.add("three");

        System.out.println("After add");
        System.out.println("the length is: " + strings.length());
        for (int i = 0; i < strings.length(); i++) {
            System.out.println(strings.get(i));
        }
        System.out.println();
        strings.remove(1);
        System.out.println("After remove");
        System.out.println("the length is: " + strings.length());
        for (int i = 0; i < strings.length(); i++) {
            System.out.println(strings.get(i));
        }
    }
    public static class DynamicArray<T> {
        T[] array = (T[]) new Object[0];
        int elArray = 0;

        public void add(T el) {
            if (elArray >= array.length) {
                array = Arrays.copyOf(array, array.length+1+(array.length/2));
                array[elArray] = el;
                elArray++;
            } else {
                array[elArray] = el;
                elArray++;
            }
        }

        public void remove(int index) {
            System.arraycopy(array, index+1, array, index, array.length-index-1);
        }

        public T get(int index) {
            return array[index];
        }

        public int length() {
            return array.length;
        }

}
}