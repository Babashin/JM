package Last;

import java.util.*;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<Sendable<T>> {
    Map<String, List<T>> mailBox = new HashMap<>() {
        @Override
        public List<T> get(Object key) {
            return super.getOrDefault(key, new LinkedList<>());
        }
    };

    public Map<String, List<T>> getMailBox() {
        return mailBox;
    }

    @Override
    public void accept(Sendable<T> tSendable) {
        List<T> list = new ArrayList<>();
        if (mailBox.containsKey(tSendable.getTo())) {
            list = mailBox.get(tSendable.getTo());
            list.add(tSendable.getContent());
        } else {
            list.add(tSendable.getContent());
        }
        mailBox.put(tSendable.getTo(), list);
    }

    @Override
    public Consumer<Sendable<T>> andThen(Consumer<? super Sendable<T>> after) {
        return Consumer.super.andThen(after);
    }
}
