package Last;

public class MailMessage extends Sendable<String> {
    public MailMessage(String from, String to, String message) {
        super(from, to, message);
    }
}
