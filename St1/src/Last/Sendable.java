package Last;

public abstract class Sendable<T> {
    private final String from;
    private final String to;
    private final T message;

    public Sendable(String from, String to, T message) {
        this.from = from;
        this.to = to;
        this.message = message;
    }

    String getFrom() {
        return from;
    }

    String getTo() {
        return to;
    }

    T getContent() {
        return message;
    }
}
