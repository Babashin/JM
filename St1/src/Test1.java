import java.io.IOException;

public class Test1 {
    public static void main(String[] args) {

    }

    public static void moveRobot(RobotConnectionManager robotConnectionManager, int toX, int toY) {
        for (int chetchikSoedinenii = 0; chetchikSoedinenii < 3; chetchikSoedinenii++) {
            try (RobotConnection rc = robotConnectionManager.getConnection();) {
                rc.moveRobotTo(toX, toY);
                break;
            } catch (RobotConnectionException robotConnectionException) {
                if (robotConnectionException.getStackTrace()[0].getMethodName().equals("close")) {
                } else if (chetchikSoedinenii > 1) {
                    throw robotConnectionException;
                }
            }
        }
    }

}


interface RobotConnection extends AutoCloseable {
    void moveRobotTo(int x, int y);

    @Override
    void close();
}

interface RobotConnectionManager {
    RobotConnection getConnection();
}

class RobotConnectionException extends RuntimeException {
    public RobotConnectionException(String message) {
        super(message);
    }

    public RobotConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}